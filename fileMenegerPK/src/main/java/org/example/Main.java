package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        FileMenager fileMeneger = new FileMenager("D:\\Program java\\File meneger rk\\fileMenegerFromInopolis\\fileMenegerPK\\root");

        String input = scanner.nextLine();

        while (!input.equals(Commands.EXIT)) {
            String tokens[] = input.split(" ");
            String command = tokens[0];
            switch (command) {
                case Commands.LISt_OF_FILES:
                    fileMeneger.listOfFiles(false);
                    break;
                case Commands.LIST_OF_FILES_WITH_SIZE:
                    fileMeneger.listOfFiles(true);
                    break;
                case Commands.COPY_FILE:
                    String sourceFileName = tokens[1];
                    String destFileName = tokens[2];
                    fileMeneger.copyFile(sourceFileName, destFileName);
                    break;
                case Commands.CREATE_FILE: {
                    String fileName = tokens[1];
                    fileMeneger.createFile(fileName);
                    break;
                }
                case Commands.FILE_CONTENT: {
                    String fileName = tokens[1];
                    fileMeneger.fileContent(fileName);
                    break;
                }
                case Commands.CHANGE_DIRECTORY:
                    String folderName = tokens[1];
                    fileMeneger.changeDirectory(folderName);
                    break;

            }
            input = scanner.nextLine();

        }

    }
}
