package org.example;

import org.apache.commons.io.FileUtils;

import java.io.*;

public class FileMenager {
    private String currentFolder;
    private String root;

    public FileMenager(String currentFolder) {
        this.currentFolder = currentFolder;
        this.root = currentFolder;

    }

    public void listOfFiles(boolean withSize) {
        File currentFolederFile = new File(currentFolder);
        File files[] = currentFolederFile.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                if (withSize) {
                    System.out.println(file.getName() + "\\ " + FileUtils.sizeOfDirectory(file));
                } else {
                    System.out.println(file.getName() + "\\ ");
                }
            } else {
                if (withSize) {
                    System.out.println(file.getName() + " " + file.length());
                } else {
                    System.out.println(file.getName());
                }
            }

        }
    }

    public void copyFile(String sourceFileName, String destFileName) {
        File source = new File(currentFolder + "\\ " + sourceFileName);
        File dest = new File(currentFolder + "\\ " + destFileName);
        System.out.println(sourceFileName);
        System.out.println(destFileName);
        try {
            FileUtils.copyFile(source, dest);
            //FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            System.err.println("произошла ошибка :(");
            e.printStackTrace();
        }
    }

    public void createFile(String fileName) {
        File file = new File(currentFolder + "\\" + fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.err.println("Что то пошло не так:");
            ;
        }
    }

    public void fileContent(String fileName) {
        File file = new File(currentFolder + "\\" + fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            System.err.println("опять что то пошло не туда и не так");;
        }

    }

    public void changeDirectory(String folderName) {
        if(folderName.equals("/")) {
            this.currentFolder = this.root;
        }else if(folderName.equals("..")) {
            int startLastFolderPosition = this.currentFolder.lastIndexOf("\\");
            this.currentFolder = this.currentFolder.substring(0, startLastFolderPosition);
        } else {
            this.currentFolder = this.currentFolder + "\\" + folderName;
        }
    }
}

